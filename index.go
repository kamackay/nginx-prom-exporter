package main

import (
	"fmt"
	"github.com/hpcloud/tail"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/push"
	"github.com/robfig/cron/v3"
	"io/ioutil"
	"os"
	"strings"
)

const (
	RootFolder = "/log"
)

type Follower struct {
	watches map[string]*func()
	promUrl string
}

func readFilesFrom(path string) []os.FileInfo {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		fmt.Println(err)
		return make([]os.FileInfo, 0)
	}
	//fmt.Printf("Found %d files in %s\n", len(files), path)
	return files
}

func (this *Follower) startTail() {
	for _, file := range readFilesFrom(RootFolder) {
		file := file
		if this.watches[file.Name()] == nil {
			reader := func() {
				fileName := fmt.Sprintf("%s/%s", RootFolder, file.Name())
				fmt.Printf("Starting Read on %s\n", fileName)
				t, err := tail.TailFile(fileName, tail.Config{Follow: true})
				if err != nil {
					fmt.Println(err)
					return
				}
				for line := range t.Lines {
					if !strings.Contains(line.Text, "kube-probe") {
						fmt.Println(line.Text)
					}
				}
			}
			go reader()
			this.watches[file.Name()] = &reader
		}
	}
}

func (this *Follower) pushMetric(line string) {
	completionTime := prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "http_request_time",
		Help: "The Time in ms that a request took to process",
	})
	completionTime.SetToCurrentTime()
	if err := push.New(this.promUrl, "http_requests").
		Collector(completionTime).
		Grouping("time", "0").
		Grouping("line", line).
		Push(); err != nil {
		fmt.Println("Could not push completion time to Pushgateway:", err)
	}
}

func main() {
	c := cron.New(cron.WithSeconds())
	follower := &Follower{
		watches: make(map[string]*func(), 0),
		promUrl: os.Getenv("PROM_URL"),
	}
	_, err := c.AddFunc("*/10 * * * * *", func() {
		follower.startTail()
	})
	if err != nil {
		fmt.Printf("%+v", err)
	}
	fmt.Println("Starting Cron")
	c.Start()
	select {} // block forever
}

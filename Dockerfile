FROM golang:alpine as builder

WORKDIR /app/
WORKDIR $GOPATH/src/gitlab.com/kamackay/nginx-prom-exporter

RUN apk upgrade --update --no-cache && \
        apk add --no-cache \
            dep \
            linux-headers \
            build-base

ADD ./go.mod ./

RUN go mod download && go mod verify

ADD ./ ./

RUN go build -o server ./*.go && cp ./server /app/

FROM alpine:latest
WORKDIR /app
COPY --from=builder /app/server /app/

CMD ["./server"]



module gitlab.com/kamackay/nginx-prom-exporter

go 1.14

require (
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/hpcloud/tail v1.0.0
	github.com/prometheus/client_golang v1.7.1
	github.com/robfig/cron/v3 v3.0.0
	gopkg.in/fsnotify.v1 v1.4.7 // indirect
	gopkg.in/tomb.v1 v1.0.0-20141024135613-dd632973f1e7 // indirect
)
